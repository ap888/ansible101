# Configuración 
1. Cree un directorio con el nombre ```proyecto2019```
2. Cree un inventario en formato YAML, registre los dos equipos con los nombres virtual1 y virtual2, el primero en el grupo web y el segundo en el grupo db, los dos grupos deben ser parte del grupo datacenter
```code
.
└── inventory
    ├── hosts.yml
    └── host_vars
        ├── virtual1
        │   └── vars.yml
        └── virtual2
            └── vars.yml

4 directories, 3 files
```
3. En el directorio raiz, cree el archivo ```ansible.cfg```, con el siguiente contenido:
```
[defaults]
inventory = inventory/hosts.yml
forks = 5

[ssh_connection]
retries = 2
```
Revisa la información de ejecución de Ansible
```
$ ansible --version

$ ansible-config view
```
Obtenga los gathers del host : localhost
```sh
$ ansible -m setup localhost
```

Pruebe : 
```code
$ ansible -m setup localhost -e ansible_connection=ssh
```
