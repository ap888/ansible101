# Entorno virtual

Copia el nuevo Vagrantfile, en la ruta donde previamente creaste tu ambiente :


```bash
cp ansible101/virtual-environment/Vagrantfile <path>/.

vagrant up --provision
```
