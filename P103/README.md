# Inventario 
1. Ingresa a la máquina virtual con nombre ansible vía ssh, usando las siguientes credenciales:

```code
user: demo
password: superdemo
```

```sh
$ ssh -l demo 192.168.10.254
```
2. Verifica la conexión a los equipos ```192.168.10.[202-203]```
3. Crea un directorio para el proyecto y dentro crea el inventario para los tres nodos hasta ahora explorados, estructura de archivos debe verse así :
```code
.
└── inventory
    ├── hosts.yml
    └── host_vars
        ├── machine001
        │   └── vars
        └── machine002
            └── vars

4 directories, 3 files
```

```bash
$ mkdir -p superansible101/inventory/host_vars/machine00{1,2}
$ cat > inventory/hosts.yml << EOF
web:
  hosts:
    machine001:
    machine002:
db:
  hosts:
    machine001:
EOF

$ cat > inventory/host_vars/machine001/vars << EOF
ansible_host: 192.168.10.202
ansible_user: demo
EOF

$ cat > inventory/host_vars/machine002/vars << EOF
ansible_host: 192.168.10.203
ansible_user: demo
EOF
```

4. Realiza la instalación de Ansible con la ayuda de PIP
```bash
	$ pip3 install ansible --user
```
5. Verifica la versión de ansible 
```bash
	$ ansible --version
```
6. Con la ayuda de **Ansible** verifique la lista de hosts en el inventario (pruebe con los grupos:  all, ungrouped)
```bash
	$ ansible -i inventory/hosts --list-host web
	$ ansible -i inventory/hosts --list-host db
```
7. Verifique la conexión a cada equipo
```bash
	$ ansible -i inventory/hosts -m ping machine001
```
