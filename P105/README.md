# Playbook
1. Cree un directorio con el nombre ```playbooks-admin```
2. Cree el inventario:
   ```yaml
   all:
    children:
      datacenter:
        children:
            web:
              hosts:
                web001:
                    ansible_host: 192.168.10.202
                    ansible_port: 22
                    ansible_user: demo
                    ansible_password: superdemo
                web002:
                    astnsible_host: 192.168.10.203
                    ansible_port: 22
                    ansible_user: demo
                    ansible_password: superdemo
            db:
              hosts:
                web001: {} 
            dev:
              hosts:
                server0001:
                  ansible_host: 192.168.10.254
                  ansible_port: 22
                  ansible_user: demo
                  ansible_password: superdemo
      ungrouped: {}
    ```
```code
.
└── inventory
    └── hosts.yml
```
1 directories, 3 files
```
1. En el directorio raiz, cree el archivo ```ansible.cfg```, con el siguiente contenido:
```

